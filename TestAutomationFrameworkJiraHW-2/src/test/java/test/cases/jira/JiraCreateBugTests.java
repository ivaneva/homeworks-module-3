package test.cases.jira;

import org.junit.Test;
import pages.jira.ProjectPage;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;


public class JiraCreateBugTests extends BaseTest {

    private String bugId;

    @Test
    public void createBugTest(){
        ProjectPage projectPage = new ProjectPage(actions.getDriver());
        projectPage.navigateToPage();
        projectPage.createIssue("bug", "medium");
        projectPage.assertIssueCreated(getConfigPropertyByKey(("jira.project.bug.summary")));
        bugId = projectPage.getIssueId();
    }

}
