package test.cases.jira;

import org.junit.Test;
import pages.jira.IssuePage;
import pages.jira.ProjectPage;

public class JiraLinkStoryAndBugTests extends BaseTest {

    @Test
    public void linkStoryAndBugTest(){
        IssuePage issuePage = new IssuePage(actions.getDriver());
        issuePage.navigateToPage();
        issuePage.linkStoryAndBug();
        issuePage.assertIssuesLinked();
    }


}
