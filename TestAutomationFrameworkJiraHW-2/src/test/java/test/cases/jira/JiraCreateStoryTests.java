package test.cases.jira;

import org.junit.Test;
import pages.jira.ProjectPage;
import static com.telerikacademy.testframework.Utils.*;

public class JiraCreateStoryTests extends BaseTest {
    private String storyId;

    @Test
    public void createStoryTest(){
        ProjectPage projectPage = new ProjectPage(actions.getDriver());
        projectPage.navigateToPage();
        projectPage.createIssue("story", "high");
        projectPage.assertIssueCreated(getConfigPropertyByKey(("jira.project.story.summary")));
        storyId = projectPage.getIssueId();
    }

}

