package com.telerikacademy.testframework;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertiesManager {

    public enum PropertiesManagerEnum {

        INSTANCE;
        private Properties configProperties = null;
        private Properties uiMappings = null;
        private static final String UI_MAP = "src/test/resources/mappings/ui_map.properties";
        private static final String CONFIG_PROPERTIES = "src/test/resources/config.properties";

        public Properties getConfigProperties() {
            return configProperties = loadProperties(CONFIG_PROPERTIES);
        }

        public Properties getUiMappings() {
            return uiMappings = loadProperties(UI_MAP);
        }

        private static Properties loadProperties(String url) {
            Properties props = new Properties();
            try {
                props.load(Files.newInputStream(Paths.get(url)));
            } catch (IOException ex) {
                Utils.LOGGER.error("Loading properties failed!");
            }
            return props;
        }
    }
}
