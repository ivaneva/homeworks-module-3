package pages.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class ProjectPage extends BaseJiraPage{
    private WebElement issueId;

    public ProjectPage(WebDriver driver){ super(driver, "jira.projectUrl"); }

    public void createIssue(String issueKey, String priority) {

        actions.waitForElementVisible("jira.projectPage.createButton");
        actions.clickElement("jira.projectPage.createButton");

        assertProjectSelected(getConfigPropertyByKey(("jira.projectName")));

        actions.clickElement("jira.project.issueType.dropDownBox");
        actions.waitForElementVisible("jira.project.issueType." + issueKey + ".select");
        actions.clickElement("jira.project.issueType." + issueKey + ".select");

        actions.waitForElementVisible("jira.project.summaryField");
        actions.typeValueInField(getConfigPropertyByKey("jira.project." + issueKey + ".summary"),
                "jira.project.summaryField");

        actions.waitForElementVisible("jira.project.descriptionField");
        actions.typeValueInField(getConfigPropertyByKey("jira.project." + issueKey + ".description"),
                "jira.project.descriptionField");

        actions.clickElement("jira.project.priority.dropDownBox");
        actions.waitForElementVisible("jira.project.priority." + priority + ".select");
        actions.clickElement("jira.project.priority." + priority + ".select");

        actions.clickElement("jira.project.createButton");
    }

    public String getIssueId(){
        issueId = actions.getDriver().findElement(
                (By.xpath("//a[@target='_blank']")));
        return issueId.getText();
    }

    public void assertProjectSelected(String projectTitle){
        actions.waitForElementVisible("jira.project.name.dropDownBox");
        actions.assertElementAttribute("jira.project.name.dropDownBox", "innerText", projectTitle);
    }

    public void assertIssueCreated(String issueTitle){
        actions.waitForElementVisible("jira.issue.view");
        actions.assertElementPresent("jira.issue.view");
        actions.clickElement("jira.issue.view");
        actions.waitForElementVisible("jira.issue.name");
        actions.assertElementAttribute("jira.issue.name", "innerText", issueTitle);
    }

}
