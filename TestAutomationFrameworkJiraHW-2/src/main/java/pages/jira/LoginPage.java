package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BaseJiraPage {

    public LoginPage(WebDriver driver) {
        super(driver, "jira.projectUrl");
    }

    public void loginUser(String userKey) {
        String username = Utils.getConfigPropertyByKey("jira." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.loginButton");

//        actions.waitFor(2000);

        actions.waitForElementVisible("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.loginButton");

        actions.waitForElementVisible("jira.projectsPage.avatar");
    }

}
