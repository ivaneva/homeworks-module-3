package pages.jira;

import net.bytebuddy.asm.Advice;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class IssuePage extends BaseJiraPage{

    WebElement issueSearchField;
    public IssuePage(WebDriver driver){ super(driver, "jira.issueUrl"); }

    public void linkStoryAndBug(){
        actions.waitForElementVisible("jira.issue.linkIssuesButton");
        actions.clickElement("jira.issue.linkIssuesButton");

        actions.waitForElementClickable("jira.issue.searchField");
        actions.clickElement("jira.issue.searchField");


        Actions actions1 = new Actions(driver);
        actions1.sendKeys(getConfigPropertyByKey("jira.issueToLink"));
        actions1.sendKeys(Keys.ENTER);
        actions1.perform();

        actions.waitForElementClickable("jira.issue.linkButton");
        actions.clickElement("jira.issue.linkButton");

    }

    public void assertIssuesLinked(){
        actions.waitForElementVisible("jira.issueslinked");
        actions.assertElementPresent("jira.issueslinked");
    }


}
