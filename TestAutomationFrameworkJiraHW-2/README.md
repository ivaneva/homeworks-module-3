**Instructions**
1. Clone repository
2. Open `TestAutomationFrameworkJiraHW-2` as a IntelliJ IDEA Project

3. FILL IN credentials, CHANGE url and issues in file config.properties: 7 fields.
   If not working, let me know and I will send my file with details.

jira.projectUrl=https://bg6.atlassian.net/jira/software/c/projects/ST/boards/10
jira.issueUrl=https://bg6.atlassian.net/browse/ST-49

jira.projectName=Selenium Tests (ST)
jira.issueToLink=ST-50

jira.user.username=
jira.user.password=



4. If the Story you try to link something to already has issues linked, the test will fail.
5. Build
6. Run tests in `src\test\java\test.cases.jira\` 