import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BingTelerikSearchTest {

    @Test
    public void searchTelerikBingTest() {

        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.navigate().to("https://www.bing.com");

        WebElement searchBar = webDriver.findElement(By.id("sb_form_q"));
        searchBar.sendKeys("Telerik Academy Alpha");
        searchBar.sendKeys(Keys.RETURN);

        WebElement firstResult = webDriver.findElement(
                By.xpath("//a[@href='https://www.telerikacademy.com/alpha']"));

        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha", firstResult.getText());

        webDriver.quit();
    }
}
