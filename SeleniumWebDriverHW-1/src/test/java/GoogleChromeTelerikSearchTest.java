import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleChromeTelerikSearchTest {

    @Test
    public void searchTelerikGoogleChromeTest() {

        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.navigate().to("https://www.google.com");

        webDriver.findElement(By.id("L2AGLb")).click();

        WebElement searchBar = webDriver.findElement(
                By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"));
        searchBar.sendKeys("Telerik Academy Alpha");
        searchBar.sendKeys(Keys.RETURN);


        WebElement firstResult = webDriver.findElement(
                By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/div/a/h3"));

        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha", firstResult.getText());

        webDriver.quit();
    }
}
